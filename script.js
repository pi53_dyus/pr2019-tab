( function( window ) {
    let hasClass, addClass, removeClass;
    if ( 'classList' in document.documentElement ) {
        hasClass = function( elem, c ) {
            return elem.classList.contains( c );
        };
        addClass = function( elem, c ) {
            elem.classList.add( c );
        };
        removeClass = function( elem, c ) {
            elem.classList.remove( c );
        };
    }
    function toggleClass( elem, c ) {
        let fn = hasClass( elem, c ) ? removeClass : addClass;
        fn( elem, c );
    }
    let classie = {
        has: hasClass,
        add: addClass,
        remove: removeClass,
        toggle: toggleClass
    };
    if ( typeof define === 'function' && define.amd ) {
        define( classie );
    } else {
        window.classie = classie;
    }
})( window );
let $ = function(selector){
    return document.querySelector(selector);
};
let accordion = $('.accordion');
accordion.addEventListener("click",function(e) {
    e.stopPropagation();
    e.preventDefault();
    if(e.target && e.target.nodeName == "BUTTON") {
        let classes = e.target.className.split(" ");
        if(classes) {
            for(let x = 0; x < classes.length; x++) {
                if(classes[x] == "tab-title") {
                    let title = e.target;
                    let content = e.target.parentNode.nextElementSibling;
                    classie.toggle(title, 'tab-titleActive');
                    if(classie.has(content, 'content-collapsed')) {
                        if(classie.has(content, 'animateOut')){
                            classie.remove(content, 'animateOut');
                        }
                        classie.add(content, 'animateIn');
                    }else{
                        classie.remove(content, 'animateIn');
                        classie.add(content, 'animateOut');
                    }
                    classie.toggle(content, 'content-collapsed');
                }
            }
        }
    }
});